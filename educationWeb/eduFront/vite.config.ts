import {fileURLToPath, URL} from "url";

import {defineConfig} from "vite";
import vue from "@vitejs/plugin-vue";
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [ vue() ],
  resolve: {
    alias: {
      // @ts-ignore
      "@": fileURLToPath(new URL("./src", import.meta.url)),
      // @ts-ignore
      "types": fileURLToPath(new URL("./src/@types", import.meta.url)),
    },
  },
  server: {
    proxy: {
      '/dart': {
        target: 'https://opendart.fss.or.kr/api',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/dart/, '')
      }
    }
  }
});
