import type {Module} from 'vuex';
import type {RootState, MemberState} from '@/store';

const INIT_STATE: any = {
  memberId: '',              // 사용자ID
  companyId: '',             // 회사ID
  id: '',                    // ID
  name: '',                  // 이름
  departmentName: '',        // 부서명
  positionName: '',          // 직급명
}

const member: Module<MemberState, RootState> = {
  namespaced: true,
  state: () => ({...INIT_STATE}),
  mutations: {
    setMemberState(state, payload) {
      Object.assign(state, payload)
    },
    resetMemberState(state) {
      Object.assign(state, {...INIT_STATE})
    },
  },

  actions: {
    setMember({commit}, data) {
      commit('setMemberState', data)
    },
    reset({commit}) {
      commit('resetMemberState')
    },
  },

  getters: {
    isSigned(state) {
      return !!state.id
    },
  }
}

export default member
