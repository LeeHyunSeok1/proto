import {createLogger, createStore} from 'vuex';
import createPersistedState from 'vuex-persistedstate'
import member from './modules/member';

export interface RootState {
  memberState: MemberState
}

export interface MemberState {
  memberId: string,              // 사용자ID
  companyId: string,             // 회사ID
  id: string,                    // ID
  name: string,                  // 이름
  departmentName: string,        // 부서명
  positionName: string,          // 직급명
}

const plugins = [
  createPersistedState({
    paths: ['member'],
  }), 
]

export const store = createStore({
  plugins: import.meta.env.DEV ? [createLogger(), ...plugins] : [...plugins],
  modules: {member}
})
