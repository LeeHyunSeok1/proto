// src/router.ts
import { createWebHistory, createRouter } from "vue-router";
import {store} from '@/store'; 
import exampleRouter from "@/router/example-router"; 
import HomeView from "@/views/HomeView.vue";
const routes = [
  exampleRouter,
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {path: '/:pathMatch(.*)*', redirect: '/error/404', meta: {allowAnonym: true}}, // or else 404
];
 
const router = createRouter({
  history: createWebHistory(),
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash,
        offset: {y: 200}
      }
    }

    // always scroll to top
    return {top: 0};
  },
  routes,
})

router.beforeEach((to, from) => {
  console.log("???");
  // 화면 접근 권한이 All 일때
  if (to.meta?.allowAnonym) return true
  console.log(!store.getters['member/isSigned']);
  // 로그인 상태 확인 로그인아 안되어 있으면 로그인 화면으로
  if (!store.getters['member/isSigned']) {
    console.log("?")
    //return '/login'
  }
  
  // 404
  if (!to.matched?.length) {
    return '/error/404'
  }

  return true
})

export default router;