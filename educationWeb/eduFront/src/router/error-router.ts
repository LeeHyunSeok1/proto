export default {
  path: '/error', name: '', component: () => import("@/views/layout/error_layout.vue"), meta: {allowAnonym: true},
  children: [
    {
      path: '',
      name: 'error',
      component: () => import("@/views/common/error/error.vue"),
      meta: {allowAnonym: true}
    },
    {
      path: '404',
      name: 'error404',
      component: () => import("@/views/common/error/404.vue"),
      meta: {allowAnonym: true}
    },
    {
      path: '500',
      name: 'error500',
      component: () => import("@/views/common/error/500.vue"),
      meta: {allowAnonym: true}
    },
  ]
}
