export default {
  path: '/template', name: 'template',  component: () => import("@/views/layout/basic_layout.vue"), meta: {title: 'template'},
  children: [
    {
      path: 'template01',
      name: 'template_template01',
      meta: {title: 'Editor / Viewer'},
      component: () => import("@/views/template/template01.vue"),
    },
    {
      path: 'template02',
      name: 'template_template02',
      meta: {title: 'PagingTable'},
      component: () => import("@/views/template/template02.vue"),
    },
    {
      path: 'template03',
      name: 'template_template03',
      meta: {title: 'InfiniteScrollTable'},
      component: () => import("@/views/template/template03.vue"),
    },
    {
      path: 'template04',
      name: 'template_template04',
      meta: {title: 'SearchTable',searchYn: true},
      component: () => import("@/views/template/template04.vue"),
    },
    {
      path: 'template05',
      name: 'template_template05',
      meta: {title: 'IndustrySelector',searchYn: true},
      component: () => import("@/views/template/template05.vue"),
    },
    {
      path: 'template06',
      name: 'template_template06',
      meta: {title: 'PostSelectPopup',searchYn: true},
      component: () => import("@/views/template/template06.vue"),
    },
  ]
}
