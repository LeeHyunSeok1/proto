import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

//npm install path 안되서 일단 대기 

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    
  },
  build: {
    outDir: "../src/main/resources/static",
  }, // 빌드 결과물이 생성되는 경로
  server: {
    proxy: {
      "/api": "http://localhost:9090",
    }, // proxy 설정
  },
  plugins: [vue()],
})
