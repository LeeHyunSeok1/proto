DROP TABLE IF EXISTS tb_board;
create table tb_board(
  ID int not null AUTO_INCREMENT,
  NAME varchar(100) not null,
  STATUS int,
  PRIMARY KEY ( ID )
);