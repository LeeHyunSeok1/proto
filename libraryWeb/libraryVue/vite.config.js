import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

//npm install path 안되서 일단 대기 

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: [
      { find: '@', replacement: path.resolve(__dirname, 'src') },
      { find: '@pages', replacement: path.resolve(__dirname, 'src/pages') },
    ]
  },
  build: {
    outDir: "../src/main/resources/static",
  }, // 빌드 결과물이 생성되는 경로
  server: {
    proxy: {
      "/api": "http://localhost:7070",
    }, // proxy 설정
  },
  plugins: [vue()],
})
